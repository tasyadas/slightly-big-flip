<?php


namespace App\Http\Requests;


use Illuminate\Http\Request;

/**
 * @author Tashya Dwi Askara Siahaan <tasyadwiaskarasiahaan@gmail.com>
 **/
interface FormRequest
{
    /**
     * @return Request
     */
    public function getParams(): Request;
}
