<?php


namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * @author Tashya Dwi Askara Siahaan <tasyadwiaskarasiahaan@gmail.com>
 **/
final class CreateDisbursementRequest extends BaseRequest
{
    /**
     * CreateDisbursementRequest constructor.
     * @param  Request $request
     *
     * @throws ValidationException
     */
    public function __construct(Request $request)
    {
        $this->validate(
            $request, [
                 'bankCode'      => 'required',
                 'accountNumber' => 'required',
                 'amount'        => 'required',
                 'remark'        => 'required'
            ]
        );

        parent::__construct($request);
    }
}
