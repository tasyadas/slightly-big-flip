<?php


namespace App\Http\Requests;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * @author Tashya Dwi Askara Siahaan <tasyadwiaskarasiahaan@gmail.com>
 **/
class BaseRequest extends BaseController implements FormRequest
{
    protected array $params;
    private Request $request;

    /**
     * BaseRequest constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->params  = $request->all();;
        $this->request = $request;
    }

    /**
     * @return Request
     */
    public function getParams(): Request
    {
        return $this->request->replace($this->params);
    }
}
