<?php


namespace App\Http\Controllers;

use App\Http\Helper\HttpClient;
use App\Models\Disbursement;
use GuzzleHttp\RequestOptions;

/**
 * @author Tashya Dwi Askara Siahaan <tasyadwiaskarasiahaan@gmail.com>
 **/
final class CheckDisbursementStatusController extends Controller
{
    private HttpClient $api;

    public function __construct(HttpClient $api)
    {
        $this->api = $api;
    }

    public function __invoke(string $id)
    {
        $disburse = Disbursement::find($id);

        if ($disburse) {
            $data = $this->api->get(sprintf('disburse/%s',$disburse->slightly_flip_id),
                [
                    RequestOptions::HEADERS => [
                        'Authorization' => sprintf('Basic %s', env('SECRET_KEY')),
                        'Accept' => 'application/json',
                    ],
                ]
            );

            if ($data['status'] !== $disburse->status) {
                $disburse->status      = $data['status'];
                $disburse->receipt     = $data['receipt'];
                $disburse->time_served = $data['time_served'];

                $disburse->save();
            }

            return $disburse;
        }

        return null;
    }
}
