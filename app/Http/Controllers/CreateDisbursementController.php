<?php


namespace App\Http\Controllers;

use App\Http\Helper\HttpClient;
use App\Http\Requests\CreateDisbursementRequest;
use App\Models\Disbursement;
use GuzzleHttp\RequestOptions;

/**
 * @author Tashya Dwi Askara Siahaan <tasyadwiaskarasiahaan@gmail.com>
 **/
final class CreateDisbursementController extends Controller
{
    private HttpClient $api;

    /**
     * CreateDisbursementController constructor.
     * @param HttpClient $api
     */
    public function __construct(HttpClient $api)
    {
        $this->api = $api;
    }

    public function __invoke(CreateDisbursementRequest $request)
    {
        $data = $this->api->post('disburse',
            [
                RequestOptions::JSON => [
                    'bank_code'      => $request->getParams()->bankCode,
                    'account_number' => $request->getParams()->accountNumber,
                    'amount'         => $request->getParams()->amount,
                    'remark'         => $request->getParams()->remark
                ],
                RequestOptions::HEADERS => [
                    'Authorization' => sprintf('Basic %s', env('SECRET_KEY')),
                    'Accept' => 'application/json',
                ],
            ]
        );

        $disbursement = new Disbursement([
            "slightly_flip_id" => $data['id'],
            "amount"           => $data['amount'],
            "status"           => $data['status'],
            "bank_code"        => $data['bank_code'],
            "account_number"   => $data['account_number'],
            "beneficiary_name" => $data['beneficiary_name'],
            "remark"           => $data['remark'],
            "receipt"          => $data['receipt'],
            "time_served"      => $data['time_served'],
            "fee"              => $data['fee']
        ]);

        $disbursement->save();

        return $disbursement;
    }
}
