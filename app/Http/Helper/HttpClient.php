<?php


namespace App\Http\Helper;

use GuzzleHttp\Client;

/**
 * @author Tashya Dwi Askara Siahaan <tasyadwiaskarasiahaan@gmail.com>
 **/
class HttpClient
{
    private Client $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => env('FLIP_ENDPOINT'), 'timeout' => 60]);
    }

    public function get(string $path, array $options = []): array
    {
        $response = $this->client->get($path, $options);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function post(string $path, array $options = []): array
    {
        $response = $this->client->post($path, $options);

        return json_decode($response->getBody()->getContents(), true);
    }
}
