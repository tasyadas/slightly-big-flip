<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @author Tashya Dwi Askara Siahaan <tasyadwiaskarasiahaan@gmail.com>
 **/
class Disbursement extends Model
{
    protected $table        = 'disbursement';
    protected $primaryKey   = 'id';

    protected $fillable     = [
        'slightly_flip_id',
        'amount',
        'status',
        'bank_code',
        'account_number',
        'beneficiary_name',
        'remark',
        'receipt',
        'time_served',
        'fee'
    ];
}
