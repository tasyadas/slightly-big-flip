# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://img.shields.io/packagist/dt/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://img.shields.io/packagist/v/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://img.shields.io/packagist/l/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Slightly-big Flip Documentation

You can try this API with Postman the domain is (https://slightly-flip.herokuapp.com)

### Create Disbursement

You can create disbursement by hit endpoint /create-disbursement
Below is the payload that you should add to your postman body with POST method

```
    bankCode:required
    accountNumber:required
    amount:required
    remark:required
```

### Check Disbursement Status

You can check your disburse status by hit endpoint /check-disbursement/{id}
